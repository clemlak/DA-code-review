/* eslint-env node, mocha */
/* global artifacts, contract, it, assert */

/**
 * This goal of this test is to check the good behavior of the addressSet library
 */

const AddressSetTest = artifacts.require('AddressSetTest');

let instance;

contract('AddressSetTest', (accounts) => {
  it('Should deploy an instance of the AddressSetTest contract', () => AddressSetTest.deployed()
    .then((contractInstance) => {
      instance = contractInstance;
    }));

  it('Should check the total number of ambassadors', () => instance.getAmbassadorsTotal()
    .then((total) => {
      assert.equal(total.toNumber(), 0, 'Total number is wrong');
    }));

  it('Should add an ambassador', () => instance.addAmbassador(accounts[0]));

  it('Should check the total number of ambassadors', () => instance.getAmbassadorsTotal()
    .then((total) => {
      assert.equal(total.toNumber(), 1, 'Total number is wrong');
    }));

  it('Should check if account 0 address is inside ambassadors', () => instance.isInside(accounts[0])
    .then((contains) => {
      assert.equal(contains, true, 'Ambassadors should contain account 0 address');
    }));

  it('Should remove an ambassador', () => instance.removeAmbassador(accounts[0]));

  it('Should check the total amount of ambassadors', () => instance.getAmbassadorsTotal()
    .then((total) => {
      assert.equal(total.toNumber(), 0, 'Total is wrong');
    }));

  it('Should check if account 0 is inside of ambassadors', () => instance.isInside(accounts[0])
    .then((isInside) => {
      assert.equal(isInside, false, 'Account 0 should not be inside');
    }));

  it('Should add to add twice account 1', () => instance.addAmbassador(accounts[1])
    .then(() => instance.addAmbassador(accounts[1]))
    .then(() => instance.getAmbassadorsTotal())
    .then((total) => {
      assert.equal(total.toNumber(), 1, 'Total is wrong');
    }));

  it('Should try to remove a non-ambassador', () => instance.removeAmbassador(accounts[2]));

  it('Should check the total number of ambassadors', () => instance.getAmbassadorsTotal()
    .then((total) => {
      assert.equal(total.toNumber(), 1, 'Total number is wrong');
    }));
});
