/* eslint-env node, mocha */
/* global artifacts, contract, it, assert */

/**
 * This file is a basic test of the DA Program smart contract.
 * The goal is to check the expected behavior of the contract.
 */

const DecentralizationAmbassadors = artifacts.require('DecentralizationAmbassadors');
const HydroToken = artifacts.require('HydroToken');
const Voting = artifacts.require('Voting');

let daInstance;
let hydroInstance;
let votingInstance;

const months = 24;
const maxAmbassadors = 100;
const payoutHydroAmount = 222222000000000000000000;

const totalAmountForAmbassadors = payoutHydroAmount * months * maxAmbassadors;

contract('DecentralizationAmbassadors', (accounts) => {
  it('Should deploy an instance of the HydroToken contract', () => HydroToken.deployed()
    .then((contractInstance) => {
      hydroInstance = contractInstance;
    }));

  it('Should deploy an instance of the DecentralizationAmbassadors contract', () => DecentralizationAmbassadors.deployed()
    .then((contractInstance) => {
      daInstance = contractInstance;
    }));

  it('Should deploy an instance of the Voting contract', () => Voting.deployed()
    .then((contractInstance) => {
      votingInstance = contractInstance;
    }));

  it('Should transfer some Hydro tokens to the DA contract', () => hydroInstance.transfer(daInstance.address, totalAmountForAmbassadors));

  it('Should check the balance of the DA contract', () => hydroInstance.balanceOf(daInstance.address)
    .then((balance) => {
      assert.equal(balance.toNumber(), totalAmountForAmbassadors, 'Balance of DA contract is wrong');
    }));

  it('Should set the DA contract address in the Voting contract', () => votingInstance.setDaAddress(daInstance.address));

  it('Should set the Voting contract address in the DA contract', () => daInstance.setVotingAddress(votingInstance.address));

  /* WARNING: This function ONLY exists in the audit test contract */
  it('Should set the address of the Hydro token contract', () => daInstance.setHydroAddress(hydroInstance.address));

  it('Should add a new ambassador from the owner account', () => daInstance.ownerAddAmbassador(accounts[1]));

  it('Should try to call recieveHydro() function from an ambassador account', () => daInstance.recieveHydro({
    from: accounts[1],
  }));

  it('Should check the balance of account 1', () => hydroInstance.balanceOf(accounts[1])
    .then((balance) => {
      assert.equal(balance.toNumber(), payoutHydroAmount, 'Balance of account 1 is wrong');
    }));

  it('Should let account 1 self-remove itself from the ambassadors list', () => daInstance.selfRemoval({
    from: accounts[1],
  }));

  it('Should prevent account 1 from calling recieveHydro() function again', () => daInstance.recieveHydro({
    from: accounts[1],
  })
    .catch((err) => {
      assert.include(err.message, 'revert', 'Nomination should get a revert error');
    }));

  it('Should let the owner add a new ambassador', () => daInstance.ownerAddAmbassador(accounts[2]));

  it('Should try to call recieveHydro() function from an ambassador account', () => daInstance.recieveHydro({
    from: accounts[2],
  }));

  it('Should check the balance of account 2', () => hydroInstance.balanceOf(accounts[2])
    .then((balance) => {
      assert.equal(balance.toNumber(), payoutHydroAmount, 'Balance of account 2 is wrong');
    }));

  it('Should let account 2 self-remove itself from the ambassadors list', () => daInstance.selfRemoval({
    from: accounts[2],
  }));

  it('Should let the owner add 10 new ambassadors', () => {
    for (let i = 1; i < 11; i += 1) {
      daInstance.ownerAddAmbassador(accounts[i]);
    }
  });

  it('Should NOT let the owner add another new ambassador', () => daInstance.ownerAddAmbassador(accounts[12])
    .catch((err) => {
      assert.include(err.message, 'revert', 'Transaction should revert');
    }));
});
