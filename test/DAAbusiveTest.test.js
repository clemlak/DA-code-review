/* eslint-env node, mocha */
/* global artifacts, contract, it, assert */

/**
 * The purpose of this test is to attack the DA contract
 * in order to perform abusive behaviors.
 *
 * This test hightlights the fact that ambassadors can call several times
 * the recieveHydro() function and withdraw an unlimited amount of tokens
 * from the DA contract.
 */

const DecentralizationAmbassadors = artifacts.require('DecentralizationAmbassadors');
const HydroToken = artifacts.require('HydroToken');
const Voting = artifacts.require('Voting');

let daInstance;
let hydroInstance;
let votingInstance;

const months = 24;
const maxAmbassadors = 100;
const payoutHydroAmount = 222222000000000000000000;

const totalAmountForAmbassadors = payoutHydroAmount * months * maxAmbassadors;

contract('DecentralizationAmbassadors', (accounts) => {
  it('Should deploy an instance of the HydroToken contract', () => HydroToken.deployed()
    .then((contractInstance) => {
      hydroInstance = contractInstance;
    }));

  it('Should deploy an instance of the DecentralizationAmbassadors contract', () => DecentralizationAmbassadors.deployed()
    .then((contractInstance) => {
      daInstance = contractInstance;
    }));

  it('Should deploy an instance of the Voting contract', () => Voting.deployed()
    .then((contractInstance) => {
      votingInstance = contractInstance;
    }));

  /* All the Hydro tokens are owned by account 0, so we give some to the DA contract */
  it('Should transfer some Hydro tokens to the DA contract', () => hydroInstance.transfer(daInstance.address, totalAmountForAmbassadors));

  it('Should check the balance of the DA contract', () => hydroInstance.balanceOf(daInstance.address)
    .then((balance) => {
      assert.equal(balance.toNumber(), totalAmountForAmbassadors, 'DA contract balance is wrong');
    }));

  it('Should set the DA contract address in the Voting contract', () => votingInstance.setDaAddress(daInstance.address));

  it('Should set the Voting contract address in the DA contract', () => daInstance.setVotingAddress(votingInstance.address));

  /* WARNING: This function ONLY exists in our test contract */
  it('Should set the address of the Hydro token contract', () => daInstance.setHydroAddress(hydroInstance.address));

  it('Should add a new ambassador from owner account', () => daInstance.ownerAddAmbassador(accounts[1]));

  it('Should check the balance of account 1', () => hydroInstance.balanceOf(accounts[1])
    .then((balance) => {
      assert.equal(balance.toNumber(), 0, 'Account 1 balance is wrong');
    }));

  it('Should send a payout from DA to account 1', () => daInstance.recieveHydro({
    from: accounts[1],
  })
    .then(() => hydroInstance.balanceOf(accounts[1]))
    .then((balance) => {
      assert.equal(balance.toNumber(), payoutHydroAmount, 'Account 1 balance is wrong');
    }));

  it('Should NOT give another payout to account 1', () => daInstance.recieveHydro({
    from: accounts[1],
  })
    .then(() => hydroInstance.balanceOf(accounts[1]))
    .then((balance) => {
      assert.equal(balance.toNumber(), payoutHydroAmount, 'Account 1 balance is wrong');
    }));

  it('Should NOT give another payout to account 1', () => daInstance.recieveHydro({
    from: accounts[1],
  })
    .then(() => hydroInstance.balanceOf(accounts[1]))
    .then((balance) => {
      assert.equal(balance.toNumber(), payoutHydroAmount, 'Account 1 balance is wrong');
    }));

  it('Should NOT give another payout to account 1', () => daInstance.recieveHydro({
    from: accounts[1],
  })
    .then(() => hydroInstance.balanceOf(accounts[1]))
    .then((balance) => {
      assert.equal(balance.toNumber(), payoutHydroAmount, 'Account 1 balance is wrong');
    }));

  it('Should NOT give another payout to account 1', () => daInstance.recieveHydro({
    from: accounts[1],
  })
    .then(() => hydroInstance.balanceOf(accounts[1]))
    .then((balance) => {
      assert.equal(balance.toNumber(), payoutHydroAmount, 'Account 1 balance is wrong');
    }));
});
