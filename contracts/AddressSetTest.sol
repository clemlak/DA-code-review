pragma solidity 0.4.24;


/**
 * This file is a custom contract made for testing ONLY
 */
library addressSet {
    struct _addressSet {
        address[] members;
        mapping(address => uint) memberIndices;
    }

    function insert(_addressSet storage self, address other) public {
        if (!contains(self, other)) {
            assert(length(self) < 2**256-1);
            self.members.push(other);
            self.memberIndices[other] = length(self);
        }
    }

    function remove(_addressSet storage self, address other) public {
        if (contains(self, other)) {
            uint replaceIndex = self.memberIndices[other];
            address lastMember = self.members[length(self)-1];
            // overwrite other with the last member and remove last member
            self.members[replaceIndex-1] = lastMember;
            self.members.length--;
            // reflect this change in the indices
            self.memberIndices[lastMember] = replaceIndex;
            delete self.memberIndices[other];
        }
    }

    function contains(_addressSet storage self, address other) public view returns (bool) {
        return self.memberIndices[other] > 0;
    }

    function length(_addressSet storage self) public view returns (uint) {
        return self.members.length;
    }
}


contract AddressSetTest {
    using addressSet for addressSet._addressSet;

    addressSet._addressSet internal ambassadors;

    function addAmbassador(address newAmbassador) external {
        ambassadors.insert(newAmbassador);
    }

    function removeAmbassador(address ambassador) external {
        ambassadors.remove(ambassador);
    }

    function getAmbassadorsTotal() external view returns (uint) {
        return ambassadors.length();
    }

    function isInside(address ambassador) external view returns (bool) {
        return ambassadors.contains(ambassador);
    }
}
