/* eslint-env node */
/* global artifacts */

/* WARNING: This migration file is for testing only and SHOULD NOT be used in production */

const DecentralizationAmbassadors = artifacts.require('DecentralizationAmbassadors');
const addressSet = artifacts.require('addressSet');
const AddressSetTest = artifacts.require('AddressSetTest');
const HydroToken = artifacts.require('HydroToken');
const Voting = artifacts.require('Voting');

function deployContracts(deployer) {
  deployer.deploy(addressSet);
  deployer.link(addressSet, DecentralizationAmbassadors);
  deployer.link(addressSet, AddressSetTest);
  deployer.deploy(AddressSetTest);

  deployer.deploy(HydroToken);
  deployer.deploy(Voting);
  deployer.deploy(DecentralizationAmbassadors);
}

module.exports = deployContracts;
