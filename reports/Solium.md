#### Solium analysis

The Solium analysis found 13 errors. However, most of them or related to syntax, style or good practices and are not mandatory.

```
contracts/DecentralizationAmbassadors.sol

  32:8      warning    Provide an error message for require().                                        error-reason
  41:8      warning    Provide an error message for require().                                        error-reason
  57:2      error      Only use indent of 4 spaces.                                        indentation
  59:6      error      Only use indent of 8 spaces.                                        indentation
  64:0      error      Only use indent of 4 spaces.                                        indentation
  69:2      error      Only use indent of 4 spaces.                                        indentation
  74:0      error      Only use indent of 4 spaces.                                        indentation
  79:2      error      Only use indent of 4 spaces.                                        indentation
  82:0      error      Only use indent of 4 spaces.                                        indentation
  87:2      error      Only use indent of 4 spaces.                                        indentation
  91:0      error      Only use indent of 4 spaces.                                        indentation
  163:8     warning    Provide an error message for require().                                        error-reason
  168:8     warning    Provide an error message for require().                                        error-reason
  178:8     warning    Provide an error message for require().                                        error-reason
  180:10    error      Only use indent of 12 spaces.                                        indentation
  181:10    error      Only use indent of 12 spaces.                                        indentation
  186:8     warning    Provide an error message for require().                                        error-reason
  193:8     warning    Provide an error message for require().                                        error-reason
  202:8     warning    Provide an error message for require().                                        error-reason
  204:10    error      Only use indent of 12 spaces.                                        indentation
  205:10    error      Only use indent of 12 spaces.                                        indentation
  217:8     warning    Provide an error message for require().                                        error-reason
  217:36    warning    There should be a maximum of single space and nocomments between left side and '>='.    operator-whitespace

✖ 13 errors, 10 warnings found.
```
