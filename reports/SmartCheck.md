#### Smart Check analysis

The analysis of the code performed through the [Smart Check](https://tool.smartdec.net/) tool found 28 results. Some of them can be ignored, such as the risk related to call of external contracts, since these contracts are related to the Hydro team.
However, some warnings must be taken seriously, especially the ones related to the unchecked math operations. For these operations, the use of the SafeMath library (which is already linked to the contract) is strongly recommended.

Here is the full result of the Smart Check analysis:

| Lines     | Severity | Finding                                  |
|-----------|----------|------------------------------------------|
| 145       | 1        | Hardcoded address                        |
| 158       | 1        | DoS by external function call in require |
| 207       | 1        | DoS by external function call in require |
| 183       | 1        | DoS by external function call in require |
| 153       | 1        | DoS by external function call in require |
| 135 - 224 | 1        | No payable fallback function             |
| 8 - 41    | 1        | No payable fallback function             |
| 1         | 2        | Compiler version not fixed               |
| 158       | 3        | Reentrancy                               |
| 186       | 3        | Reentrancy                               |
| 194       | 3        | Reentrancy                               |
| 183       | 3        | Reentrancy                               |
| 207       | 3        | Reentrancy                               |
| 170       | 3        | Reentrancy                               |
| 162       | 3        | Reentrancy                               |
| 200       | 3        | Reentrancy                               |
| 177       | 3        | Reentrancy                               |
| 159       | 3        | Reentrancy                               |
| 108       | 1        | Unchecked math                           |
| 97        | 1        | Unchecked math                           |
| 212       | 1        | Unchecked math                           |
| 97        | 1        | Unchecked math                           |
| 106       | 1        | Unchecked math                           |
| 210       | 1        | Unchecked math                           |
| 109       | 1        | Unchecked math                           |
| 143       | 0        | Implicit visibility level                |
| 145       | 0        | Implicit visibility level                |
| 146       | 0        | Implicit visibility level                |
| 142       | 0        | Implicit visibility level                |
