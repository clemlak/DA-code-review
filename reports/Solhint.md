#### Solhint analysis

The Solhint analysis found a total of 44 problems. Most of them are linked to syntax or style and do not represent a security risk.
However, the warning concerning the unfixed compiler version must be taken seriously and fixed.

Here is the full result of the Solhint analysis:

```
contracts/DecentralizationAmbassadors.sol

    1:17  warning  Compiler version must be fixed                                   compiler-fixed
   13:1   error    Definition must be surrounded with two blank line indent                                two-lines-top-level-separator
   24:5   error    Definitions inside contract / library must be separated by one line                     separate-by-one-line-in-contract
   52:1   error    Definition must be surrounded with two blank line indent                                two-lines-top-level-separator
   57:3   error    Expected indentation of 4 spaces but found 2                                   indent
   58:5   error    Expected indentation of 8 spaces but found 4                                   indent
   59:7   error    Expected indentation of 12 spaces but found 6                                   indent
   60:5   error    Expected indentation of 8 spaces but found 4                                   indent
   61:5   error    Expected indentation of 8 spaces but found 4                                   indent
   62:5   error    Expected indentation of 8 spaces but found 4                                   indent
   63:5   error    Expected indentation of 8 spaces but found 4                                   indent
   64:3   error    Expected indentation of 4 spaces but found 2                                   indent
   69:3   error    Expected indentation of 4 spaces but found 2                                   indent
   71:5   error    Expected indentation of 8 spaces but found 4                                   indent
   73:5   error    Expected indentation of 8 spaces but found 4                                   indent
   74:3   error    Expected indentation of 4 spaces but found 2                                   indent
   79:3   error    Expected indentation of 4 spaces but found 2                                   indent
   80:5   error    Expected indentation of 8 spaces but found 4                                   indent
   81:5   error    Expected indentation of 8 spaces but found 4                                   indent
   82:3   error    Expected indentation of 4 spaces but found 2                                   indent
   87:3   error    Expected indentation of 4 spaces but found 2                                   indent
   88:5   error    Expected indentation of 8 spaces but found 4                                   indent
   89:5   error    Expected indentation of 8 spaces but found 4                                   indent
   90:5   error    Expected indentation of 8 spaces but found 4                                   indent
   91:3   error    Expected indentation of 4 spaces but found 2                                   indent
   94:1   error    Definition must be surrounded with two blank line indent                                two-lines-top-level-separator
   94:9   error    Contract name must be in CamelCase                                   contract-name-camelcase
   95:12  error    Contract name must be in CamelCase                                   contract-name-camelcase
  130:1   error    Definition must be surrounded with two blank line indent                                two-lines-top-level-separator
  135:1   error    Definition must be surrounded with two blank line indent                                two-lines-top-level-separator
  140:1   error    Definition must be surrounded with two blank line indent                                two-lines-top-level-separator
  147:5   warning  Explicitly mark visibility of state                                   state-visibility
  148:5   warning  Explicitly mark visibility of state                                   state-visibility
  150:5   warning  Explicitly mark visibility of state                                   state-visibility
  151:5   warning  Explicitly mark visibility of state                                   state-visibility
  180:11  error    Expected indentation of 12 spaces but found 10                                   indent
  181:11  error    Expected indentation of 12 spaces but found 10                                   indent
  204:11  error    Expected indentation of 12 spaces but found 10                                   indent
  205:11  error    Expected indentation of 12 spaces but found 10                                   indent
  217:40  error    Expression indentation is incorrect. Required space before >=                           expression-indent
  220:66  error    Open bracket must be on same line. It must be indented by other constructions by space  bracket-align
  222:17  warning  Possible reentrancy vulnerabilities. Avoid state changes after transfer                 reentrancy
  228:5   warning  Event and function names must be different                                   no-simple-event-func-name
  230:5   warning  Event and function names must be different                                   no-simple-event-func-name

✖ 44 problems (36 errors, 8 warnings)
```
