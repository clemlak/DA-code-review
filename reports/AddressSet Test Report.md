#### AddressSet Test Report

A dynamic test has been performed on the `AddressSet` library. The library is working as intended and the only recommendation is the following:
- The `if` statement line [104](https://https://github.com/hydrogen-dev/smart-contracts/blob/master/decentralization-ambassadors/DA.sol#L104) could be replaced by a `revert`, in order to warn about the failure of the function.

```
Contract: AddressSetTest
  ✓ Should deploy an instance of the AddressSetTest contract
  ✓ Should check the total number of ambassadors (648ms)
  ✓ Should add an ambassador (3070ms)
  ✓ Should check the total number of ambassadors
  ✓ Should check if account 0 address is inside ambassadors
  ✓ Should remove an ambassador (102ms)
  ✓ Should check the total amount of ambassadors (339ms)
  ✓ Should check if account 0 is inside of ambassadors (409ms)
  ✓ Should add to add twice account 1 (3819ms)
  ✓ Should try to remove a non-ambassador (518ms)
  ✓ Should check the total number of ambassadors


11 passing (9s)
```
