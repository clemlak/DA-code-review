#### Test report of the `DAAbusiveTest.test.js` test

The goal of this test was to hightlight the critical issue within the DA smart contract.
As shown below, the ambassador (account 1) was able to call the `recieveHydro()` function several times in a row and withdraw a lot of amount from the DA contract balance.
This issue is critical and should be fixed as soon as possible, as proposed in the **Findings** section.

Here is the full test report:

```
Contract: DecentralizationAmbassadors
    ✓ Should deploy an instance of the HydroToken contract
    ✓ Should deploy an instance of the DecentralizationAmbassadors contract
    ✓ Should deploy an instance of the Voting contract
    ✓ Should transfer some Hydro tokens to the DA contract (786ms)
    ✓ Should check the balance of the DA contract
    ✓ Should set the DA contract address in the Voting contract (1124ms)
    ✓ Should set the Voting contract address in the DA contract (1270ms)
    ✓ Should set the address of the Hydro token contract (94ms)
    ✓ Should add a new ambassador from owner account (3020ms)
    ✓ Should check the balance of account 1 (546ms)
    ✓ Should send a payout from DA to account 1 (2131ms)
    1) Should NOT give another payout to account 1

    Events emitted during test:
    ---------------------------

    Transfer(_from: <indexed>, _to: <indexed>, _amount: 2.22222e+23)
    Payout(_ambassador: 0xa1b2bf796bab48c7480cb1286ef3a6e9f79b9fdd, _amount: 2.22222e+23)

    ---------------------------
    2) Should NOT give another payout to account 1

    Events emitted during test:
    ---------------------------

    Transfer(_from: <indexed>, _to: <indexed>, _amount: 2.22222e+23)
    Payout(_ambassador: 0xa1b2bf796bab48c7480cb1286ef3a6e9f79b9fdd, _amount: 2.22222e+23)

    ---------------------------
    3) Should NOT give another payout to account 1

    Events emitted during test:
    ---------------------------

    Transfer(_from: <indexed>, _to: <indexed>, _amount: 2.22222e+23)
    Payout(_ambassador: 0xa1b2bf796bab48c7480cb1286ef3a6e9f79b9fdd, _amount: 2.22222e+23)

    ---------------------------
    4) Should NOT give another payout to account 1

    Events emitted during test:
    ---------------------------

    Transfer(_from: <indexed>, _to: <indexed>, _amount: 2.22222e+23)
    Payout(_ambassador: 0xa1b2bf796bab48c7480cb1286ef3a6e9f79b9fdd, _amount: 2.22222e+23)

    ---------------------------


  11 passing (10s)
  4 failing

  1) Contract: DecentralizationAmbassadors
       Should NOT give another payout to account 1:
     AssertionError: Account 1 balance is wrong: expected 4.44444e+23 to equal 2.22222e+23
      at daInstance.recieveHydro.then.then (test/DAAbusiveTest.test.js:78:14)
      at <anonymous>
      at process._tickCallback (internal/process/next_tick.js:188:7)

  2) Contract: DecentralizationAmbassadors
       Should NOT give another payout to account 1:
     AssertionError: Account 1 balance is wrong: expected 6.66666e+23 to equal 2.22222e+23
      at daInstance.recieveHydro.then.then (test/DAAbusiveTest.test.js:86:14)
      at <anonymous>
      at process._tickCallback (internal/process/next_tick.js:188:7)

  3) Contract: DecentralizationAmbassadors
       Should NOT give another payout to account 1:
     AssertionError: Account 1 balance is wrong: expected 8.88888e+23 to equal 2.22222e+23
      at daInstance.recieveHydro.then.then (test/DAAbusiveTest.test.js:94:14)
      at <anonymous>
      at process._tickCallback (internal/process/next_tick.js:188:7)

  4) Contract: DecentralizationAmbassadors
       Should NOT give another payout to account 1:
     AssertionError: Account 1 balance is wrong: expected 1.11111e+24 to equal 2.22222e+23
      at daInstance.recieveHydro.then.then (test/DAAbusiveTest.test.js:102:14)
      at <anonymous>
      at process._tickCallback (internal/process/next_tick.js:188:7)
```
