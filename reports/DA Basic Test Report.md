#### DA Basic Test Report

The goal of this test was to perform a basic / expected behavior of the DA smart contract.

```
Contract: DecentralizationAmbassadors
  ✓ Should deploy an instance of the HydroToken contract
  ✓ Should deploy an instance of the DecentralizationAmbassadors contract
  ✓ Should deploy an instance of the Voting contract
  ✓ Should transfer some Hydro tokens to the DA contract (1271ms)
  ✓ Should check the balance of the DA contract
  ✓ Should set the DA contract address in the Voting contract (909ms)
  ✓ Should set the Voting contract address in the DA contract (1196ms)
  ✓ Should set the address of the Hydro token contract
  ✓ Should add a new ambassador from the owner account (3934ms)
  ✓ Should try to call recieveHydro() function from an ambassador account (1323ms)
  ✓ Should check the balance of account 1
  ✓ Should let account 1 self-remove itself from the ambassadors list(47ms)
  ✓ Should prevent account 1 from calling recieveHydro() function again (664ms)
  ✓ Should let the owner add a new ambassador (3165ms)
  ✓ Should try to call recieveHydro() function from an ambassador account (2156ms)
  ✓ Should check the balance of account 2 (99ms)
  ✓ Should let account 2 self-remove itself from the ambassadors list(59ms)
  ✓ Should let the owner add 10 new ambassadors
  ✓ Should NOT let the owner add another new ambassador (76ms)


19 passing (35s)
```
